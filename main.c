/* Philip Haubrich 8/2/2014
Playing with a_life. Again.
Modelling bat infections and spread
Released to the public domain
*/

/*
Map Variables:
  -food available in winter/summer
  
  
Bat Variables:
  -maxFat
  -maxAge
  -age = decline health?
  -Young = low health?
  -birthRate.
  -Distance that bats travel
  -DeathRate from all other shit, out in the 
  -So.... they die on cold nights, right?
  -
  
Fungus Variables:
  -Survive outside of caves?

  
  
  

Map: 2D grid representing the USA. 
  Cave/non-cave/ water
  Batting quality, how much food there is/how well bats do there.   Divided amoungst the bat population. (hence they'll want to spread)
  Winter length. Center-point =  Jan 15th,  so the north is colder than the south.  
  infected  -discreet yes/no at this point.
  
  //TODO LATER
  R: Cave/non-cave/water
  B: winter length, days
  G: food
  
  
Bat:
  x/y     -Where they at
  Age     -Affects maxHealth. The very young are at risk. The old slowly decline.
  Health  -Goes up when fed. Down when hungry. Dead at zero. 
  Fat     -up in summer when they eat, declines in winter, plummets when infected
  infected  -yes/no at this point

  
Program premise:
Init: Load map from picture: cave/non-cave/water,  put a bunch of bats in the caves, it's spring

Loop:
  1) in spring bats disperse to find feeding grounds
  2) in summer they gorge, building fat
  3) in fall they go back to a cave
  4) in winter they hibernate in the cave, living off the built up fat
  
  5) Infected bats burn fat too fast and go hunting in winter, killing most. 
  
  ? in spring or summer they have babies, who have to grow up
  ? So, do bats know where other caves are?
  ? Yeah, how to bats travel around?
  
  6) Any bat in an infected tile has a chance to get infected
  7) Any infected bat has a chance to infect the tile.
  
*/

//The headers
#include <time.h>
#include <math.h>
#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "SDL/SDL_ttf.h"

#define uint32 unsigned int


#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

//Screen attributes
const int SCREEN_WIDTH = 512;  //Currently needs space to the right for text
const int SCREEN_HEIGHT = 256;
const int SCREEN_BPP = 32;

#define MAP_SIZE_X 256
#define MAP_SIZE_Y 256



const int FRAMES_PER_SECOND = 40;

#define BUFF_SIZE 1000
 

#define MAP_NOTCAVE   0 
#define MAP_CAVE      1
#define MAP_WATER     2
 
#define STATE_NORMAL    0
#define STATE_DEAD      1
#define STATE_HUNGRY    2
#define STATE_HIBERNATE 3

//Food-based constants
#define FOOD_AMOUNT 3 //How much food a tile has. It's divided amongst the bats in the tile.  
#define STARVING 20
#define MAX_FAT 120   //winter is 3 months right now, or 90 days. 

#define INFECTION_CHANCE 10   //10% chance, EACH DAY to infect the cave or the bat
#define INFECTION_EFFECT 1    // how much fat the infection burns

#define BIRTH_RATE 25  //percent chance they make one offspring each year, yeah apparently bats just have one kid at a time. Also, I'm not dealing with sexes or anything.


//Colors for a heatmap
#define HEATMAPSIZE 15
const int HEATMAP[HEATMAPSIZE][2] = {
{0,  0x00000000},
{1,  0x000022DD},
{2,  0x000044BB},
{3,  0x00006699},
{4,  0x00008877},
{5,  0x0000AA55},
{6,  0x0000CC33},
{7,  0x0000FF00},
{10, 0x0022DD00},
{20, 0x0044BB00},
{30, 0x00669900},
{40, 0x00887700},
{50, 0x00AA5500},
{100,0x00CC3300},
{500,0x00FF0000}};


//The surfaces
SDL_Surface *g_screen = NULL;     //Main surface displayed  
SDL_Surface *g_message = NULL;    //Used for text
SDL_Surface *g_png = NULL;    //the png image for the initial map values

//Font
TTF_Font *g_font;
SDL_Color textColor = { 254, 255, 255 }; // R, G, B

//The event structure
SDL_Event g_event;

char g_buff[BUFF_SIZE] = " ";

int g_time=0;
int g_paused = 0;
int g_selection=0;
int g_batSelection=1;
int g_debug=1;
int g_gameSpeed = 30;
int g_maxBatCount=1;
int g_totalBatCount=0;

Uint32 getPixel_32(SDL_Surface* surface, Uint32 x, Uint32 y);



//DATA
struct Map
{
  char type; //Red Value. Cave/non-cave/ water
  char infected; //  -discreet yes/no at this point.
  
  //Working values
  int count; //Useful for drawing, yes, we count the bats each time
  int countInfected;
 
  //TODO Latter
  //char qual; //Green value. Batting quality, how much food there is/how well bats do there.   Divided amoungst the bat population. (hence they'll want to spread)
  //char cold; //Blue value. Winter length. Center-point =  Jan 15th,  so the north is colder than the south.  
  
  int distToCave; //With a static map, we can do all pathfinding once at the start
  
} g_map[MAP_SIZE_X][MAP_SIZE_Y];


struct Bat
{
  int x;
  int y; //x/y     -Where they at
  int fat;        // up in summer when they eat, declines in winter, plummets when infected.   uh.... 0-100.  Let's say, the unit is days-worth-food
  int infected;   //-yes/no at this point
  /* //meh, latter
  Age     -Affects maxHealth. The very young are at risk. The old slowly decline.
  Health  -Goes up when fed. Down when hungry. Dead at zero. 
  */
  int state; // 0=normal, Dead, hungry, hibernate, 
};




#define MAX_NUM_BATS 100000
//God I am so fucking lazy. I should really whip out GTK and use their g_slist_alloc() , but you know? I just don't feel like mucking with the build. 
struct Bat g_bats[MAX_NUM_BATS];



int load_files()
{
  g_font = TTF_OpenFont( "Comic.ttf", 24);
  if( g_font == NULL )
  {
    printf("TTF_OpenFont fails: %s\n", TTF_GetError());
    return -1;
  }
  
  g_png = IMG_Load("map.png");
  if(!g_png)
  {
    printf("IMG_Load: %s\n", IMG_GetError());
    return -1;
  }
  return 0;
}

void apply_surface( int x, int y, SDL_Surface* source, SDL_Surface* destination )
{
  SDL_Rect offset;
  SDL_Rect* clip = NULL;
  offset.x = x;
  offset.y = y;
  SDL_BlitSurface( source, clip, destination, &offset );
}

Uint32 animation( );

/** recursive function to update the distance map.
*/
void updateDist(int x, int y, int dist)
{
  //Total debugging cruft
  //animation();
  //for(i=0; i<10000; i++);

  if(x<0 || x>=MAP_SIZE_X || y<0 || y>=MAP_SIZE_Y || dist > (MAP_SIZE_X+MAP_SIZE_Y))
    return;
  if(g_map[x][y].distToCave > dist)
  {
    g_map[x][y].distToCave = dist;
    updateDist(x,y-1,dist+1);
    updateDist(x+1,y,dist+1);
    updateDist(x-1,y,dist+1);
    updateDist(x,y+1,dist+1);
  }
}


/** Load the map data from the image data
r = type
b = cold
g = food
Everything but one cave starts uninfected

uses g_png,g_map
*/
void initMap()
{
  int x,y;
  int numberOfInfectedCaves=1;
  for(y=0; y<MAP_SIZE_Y; y++)
  {
    for(x=0; x<MAP_SIZE_X; x++)
    {
      g_map[x][y].distToCave = 512;
      g_map[x][y].infected = 0;
      g_map[x][y].count =0;
      g_map[x][y].countInfected=0;
    
    
      //I want to load the map of the cave areas from an actual map.... once it's debugged
      Uint32 color = getPixel_32(g_png, x, y);
      Uint32 red;
      //Uint32 green;
      Uint32 blue;
      if(color == 0)
        continue;
         
      blue  =  color & 0x000000ff;
      //green = (color & 0x0000ff00)>>8;
      red   = (color & 0x00ff0000)>>16;
      //TODO assume a 1:1 relationship between the map the the image for now
      //TODO also, do something less lame for the .type

      if( red > 0x44)
      {
        g_map[x][y].type = MAP_CAVE;
        if(numberOfInfectedCaves)
        {
          g_map[x][y].infected = 1;
          numberOfInfectedCaves--;
        }
      }
      
      if( blue > 0x44)
      {
        g_map[x][y].type = MAP_WATER;
      }
      
      
      /*
      if(rand()%500 == 0)
      {
        g_map[x][y].type = MAP_CAVE;
        if(numberOfInfectedCaves)
        {
          g_map[x][y].infected = 1;
          numberOfInfectedCaves--;
        }

      }
      else g_map[x][y].type = MAP_NOTCAVE;
      */
      
      //g_map[x][y].qual = green;
      //g_map[x][y].cold = blue;
    }
  }
  
  //2nd pass to calculate the distance to cave. Makes pathfinding quick and easy for bats
  for(x=0; x<MAP_SIZE_X; x++)
  {
    for(y=0; y<MAP_SIZE_Y; y++)
    {
      if(g_map[x][y].type == MAP_CAVE)
        updateDist(x,y, 0);
    }
  }
}

/** Bat's initial values. Like... where they start
 Uhhhhhh, let's randomize their starting location. Sure. 
*/
void initBats()
{
  int i;
  for(i=0; i< MAX_NUM_BATS; i++)
  {    
    g_bats[i].x = rand() % MAP_SIZE_X;
    g_bats[i].y = rand() % MAP_SIZE_Y;
    g_bats[i].fat = 20;
    g_bats[i].infected = 0;
    g_bats[i].state = STATE_NORMAL ;
    if(rand()%2)
      g_bats[i].state = STATE_DEAD ;
  }

}

int init()
{
  if( SDL_Init( SDL_INIT_EVERYTHING ) == -1 )
  {
     return -1;
  }
  g_screen = SDL_SetVideoMode( SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_BPP, SDL_SWSURFACE );
  if( g_screen == NULL )
  {
     return -1;
  }
  SDL_WM_SetCaption( "Batty", NULL );
  
  //Initialize SDL_ttf
  if( TTF_Init() == -1 )
  {
     return -1;
  }

  if( IMG_Init(IMG_INIT_PNG) != IMG_INIT_PNG)
  {
    printf("IMG_Init: Failed to init png support!\n");
     return -1;
  }
  
  srand(time(NULL));

  if( load_files() == -1 )
  {
     return -1;
  }
  
  initMap();
  initBats();
  
  return 0;
}



void clean_up()
{
  //Free the surfaces
  SDL_FreeSurface( g_screen );
  SDL_FreeSurface( g_png );
  
  //Close the font that was used
  TTF_CloseFont( g_font );

  //Quit SDL_ttf
  TTF_Quit();
  IMG_Quit();
  SDL_Quit();
}



//ALWAYS implemented EXACLY unlike how I expect it to work.
//Was that so fucking hard? I'm making a god damned mobius and my tools are stabbing me right in the back when I'm not looking. What the hell. Who fucking implements the mod-op to return negative values. I will find you and flay you! Truncated division, you're on the list.
int philMod(int val, int mod)
{
  if(val%mod < 0)
  {
    //val = mod-(val%mod);
    return mod+(val%mod);
  }
  return val % mod;
}

Uint32 getPixel_32(SDL_Surface* surface, Uint32 x, Uint32 y)
{
  Uint32 bpp;
  bpp = surface->format->BytesPerPixel;
  Uint8* pixeladdress;
  pixeladdress = (Uint8*)surface->pixels;
  pixeladdress += (y * surface->pitch) + (x *bpp);
  return *(Uint32*)pixeladdress;
}

void drawPixel(SDL_Surface* surface, Uint32 x, Uint32 y, Uint32 color)
{
  Uint32 bpp, offset;

  if(y >= SCREEN_HEIGHT) { return; }  //hmmmm, sorta ties it to g_screen... but meh
  if(x >= SCREEN_WIDTH) { return; }

  bpp = surface->format->BytesPerPixel;
  offset = surface->pitch*y + x*bpp;

  SDL_LockSurface(surface);
  memcpy(surface->pixels + offset, &color, bpp);
  SDL_UnlockSurface(surface);
}

void drawLine(SDL_Surface* surface, int x1,int y1,int x2,int y2, Uint32 color)
{
  int dx, dy, i, e;
  int incx, incy, inc1, inc2;
  int x,y;

  dx = x2 - x1;
  dy = y2 - y1;

  if(dx < 0) { dx = -dx; }
  if(dy < 0) { dy = -dy; }
  incx = 1;
  if(x2 < x1) { incx = -1; }
  incy = 1;
  if(y2 < y1) { incy = -1; }
  x=x1;
  y=y1;

  if(dx > dy)
  {
    drawPixel(surface,x,y, color);
    e = 2*dy - dx;
    inc1 = 2*( dy -dx);
    inc2 = 2*dy;
    for(i = 0; i < dx; i++)
    {
      if(e >= 0)
      {
        y += incy;
        e += inc1;
      }
      else { e += inc2; }
      x += incx;
      drawPixel(surface,x,y, color);
    }
  }
  else
  {
    drawPixel(surface,x,y, color);
    e = 2*dx - dy;
    inc1 = 2*( dx - dy);
    inc2 = 2*dx;
    for(i = 0; i < dy; i++)
    {
      if(e >= 0)
      {
        x += incx;
        e += inc1;
      }
      else { e += inc2; }
      y += incy;
      drawPixel(surface,x,y, color);
    }
  }
}




/**  Count the bats, put it in g_map[x][y].count
Alternatively we could simply add/subtract when bats move.... meh
*/
int countBats()
{
  int x,y,i;
  int totalCount=0;
  
  //Because speed be damned.
  for(x=0; x<MAP_SIZE_X; x++)
  {
    for(y=0; y<MAP_SIZE_Y; y++)
    {
      g_map[x][y].count = 0;
      g_map[x][y].countInfected = 0;
    }
  }
  g_maxBatCount = 1;
  
  for(i=0; i<MAX_NUM_BATS; i++)
  {
    if(g_bats[i].state == STATE_DEAD)
      continue;
    totalCount++;
    x = g_bats[i].x;
    y = g_bats[i].y;
    
    g_map[x][y].count++;
    
    if(g_bats[i].infected)
      g_map[x][y].countInfected++;
    
    if(g_map[x][y].count > g_maxBatCount)
      g_maxBatCount = g_map[x][y].count;
  }
  return totalCount;
}

int isWinter()
{
  return (g_time/30 % 12) > 9;
}

int isStartOfSpring()
{
  return (g_time % (12*30)) == 0;
}
 
void haveBabies()
{
  int i,j;
  
  j=0;
  for(i=0; i<MAX_NUM_BATS; i++)
  {
    if(g_bats[i].state == STATE_DEAD)
      continue;
    //Not dealing with sexes, the birth-root-thing, or odds of getting pregnant. No, this is just simplier
    if(rand()%(100/BIRTH_RATE) == 0)  
    {
      //Go through the list and find a dead slot,   detail: we only increment j once through
      for( ; j<MAX_NUM_BATS; j++)
      {
        if(g_bats[j].state != STATE_DEAD)
          continue;
        g_bats[j].state = STATE_NORMAL;
        g_bats[j].x = g_bats[i].x;
        g_bats[j].y = g_bats[i].y;
        g_bats[j].fat = g_bats[i].fat/4; //Uh.... let's say the health of the mother factors in
        g_bats[j].infected = 0;
        break;
      }
    }
  }
}


/** Bats seek out the nearest cave, and move one square in that direction
* Make a djiskta's path-map once at bootup, and then have all the bats reference their local 8 adjacent tiles
*/
void batGoCave(int i)
{
  int x,y;
  int dist;

  x = g_bats[i].x;
  y = g_bats[i].y;
  
  //Ugh, edges.... and I'm just all out of care.
  if(x == 0)            g_bats[i].x = 1;
  if(x == MAP_SIZE_X-1) g_bats[i].x = MAP_SIZE_X-2;
  if(y == 0)            g_bats[i].y = 1;
  if(y == MAP_SIZE_Y-1) g_bats[i].y = MAP_SIZE_Y-2;
  
  x = g_bats[i].x;
  y = g_bats[i].y;
  dist = g_map[x][y].distToCave;

  if(g_map[x][y+1].distToCave < dist) g_bats[i].y++;
  else if(g_map[x+1][y].distToCave < dist) g_bats[i].x++;
  else if(g_map[x][y-1].distToCave < dist) g_bats[i].y--;
  else if(g_map[x-1][y].distToCave < dist) g_bats[i].x--;
 
}

void batFeed(int i)
{
  //EAT
  int food=0, x,y;
  x = g_bats[i].x;
  y = g_bats[i].y;  
  if(g_map[x][y].count != 0)
    food = FOOD_AMOUNT/g_map[x][y].count;  
  else
    food = FOOD_AMOUNT;
  if( isWinter() )
    food = FOOD_AMOUNT /10;
  g_bats[i].fat = MIN(g_bats[i].fat + food, MAX_FAT+rand()%20);
}

//They look at the surrounding area and go to the place with the least amount of bats. 
//TODO and the most food. 
void batHunt(int i)
{ 
  int x,y, tries=5;
  int oldx, oldy;
  
  //get random adjacent square, check if has less bats (and more food)
  while(tries)
  {
    tries--;
    x = g_bats[i].x + (rand()%3) -1; 
    y = g_bats[i].y + (rand()%3) -1; 
    if(x<0 || x>=MAP_SIZE_X || y<0 || y>=MAP_SIZE_Y)
      continue;
    if(g_map[x][y].type == MAP_WATER)
      continue;
    oldx = g_bats[i].x;
    oldy = g_bats[i].y;
    if(g_map[x][y].count < g_map[oldx][oldy].count)
    {
      tries =0;  
      //Move to x,y
      g_bats[i].fat--; //Cost energy to get places.
      g_bats[i].x = x;
      g_bats[i].y = y;
      //TODO  reeeeeeaaaaaal easy to just update the mapCount right here....
    }
  }
  
  batFeed(i);
 
}

int advanceTime()
{
  static int oldTime = 0;
  if( SDL_GetTicks() - oldTime < 1000/ g_gameSpeed)
  { return 0; }
  oldTime = SDL_GetTicks();
  
  if(g_paused)
    return 0;
  
  int i, x, y;
  /*
  1) Advance day
  2) Bats feed
  3) Bats burn fat
  4) Bats move
  5) kill bats
  */
  g_time++;
  
  for(i=0; i<MAX_NUM_BATS; i++)
  {
    x = g_bats[i].x;
    y = g_bats[i].y;
    
    
    if(g_bats[i].state == STATE_DEAD)
        continue;
    
    //SOME safety checks
    if(x<0 || y< 0 || x>=MAP_SIZE_X || y >= MAP_SIZE_Y)
    {
      printf("yeah, we've got a bat off the reservation. That's gonna be all sorts of problems\nbat:%d @%d,%d\n",i,x,y);
      return -1;
    }
    
/*Bats are controlled by....    state machine? neural network?   First the easy path:
if hungry -> Hunt 
  if surrounding have less bats, Go there
if winter -> 
  if at cave -> hibernate (ie, do nothing)
  else goto nearest cave
else hunt 

*/
    if(  g_bats[i].fat < STARVING)
    {
      batHunt(i);
    }
    else if( isWinter() ) //Uh, sure that's winter for now
    {
      if(g_map[x][y].type == MAP_CAVE)
        g_bats[i].state = STATE_HIBERNATE;
      else
        batGoCave(i);
    }
    else
    {
      batHunt(i);
    }
    
    //Then, the effects of the world happen:   They burn fat, maybe get infected, maybe die
    //They burn more in winter if they're not hibernating
    if( isWinter() && g_bats[i].state != STATE_HIBERNATE )
    {
      g_bats[i].fat--;
    }
    
    //They burn if they're infected, and they have a chance to infect the cave they're in
    if(g_bats[i].infected)
    {
      g_bats[i].fat -= INFECTION_EFFECT;
      if(g_map[x][y].type == MAP_CAVE && rand()%(100/INFECTION_CHANCE) == 0)
      {
        g_map[x][y].infected = 1;
      }
    }
    
    //BURN, all the time, just to live
    g_bats[i].fat--;
    
    //And they die if they starve
    if(g_bats[i].fat < 0)
      g_bats[i].state = STATE_DEAD;
      
    //Infected caves spread to the bats
    if(g_map[x][y].infected && rand()%(100/INFECTION_CHANCE) == 0)
    {
      g_bats[i].infected = 1;
    }
  } 
  
  if(isStartOfSpring())
  {
    haveBabies();
  }

  g_totalBatCount = countBats();
  return 0;
}

Uint32 getHeatMap(int val)
{
  int i;
  for(i=0; i<HEATMAPSIZE; i++)
  {
    if(HEATMAP[i][0] >= val)
      return HEATMAP[i][1];
  }
  return HEATMAP[HEATMAPSIZE-1][1];
}

Uint32 animation( )
{
  static int oldTime = 0;
  int x,y; 
  
  if( SDL_GetTicks() - oldTime < 1000/ FRAMES_PER_SECOND)
  { return 0; }
  oldTime = SDL_GetTicks();

  //Fill the screen with black / erase the last frame
  SDL_Rect dstrect = {0,0,SCREEN_WIDTH,SCREEN_HEIGHT};
  SDL_FillRect(g_screen,&dstrect, 0x000000); 



  for(x=0; x<MAP_SIZE_X; x++)
  {
    for(y=0; y<MAP_SIZE_Y; y++)
    {
      #define SELECTIONMAX 4
      switch(g_selection)
      {
        case 0:
          //Draw bats with a lame heat-map-thing
          drawPixel(g_screen, x,y, getHeatMap(g_map[x][y].count));
        
          //Draw the caves, green = good cave, red = infected
          if(g_map[x][y].type == MAP_CAVE)
            drawPixel(g_screen, x,y, 0x0000FF00);   
          if(g_map[x][y].type == MAP_WATER)
            drawPixel(g_screen, x,y, 0x000000FF);   
          if(g_map[x][y].infected)
            drawPixel(g_screen, x,y, 0x00FF0000);   
        break;
        case 1:              
          //just the bats
          drawPixel(g_screen, x,y, getHeatMap(g_map[x][y].count)); 
        break;
        case 2:              
          //Infected bats
          drawPixel(g_screen, x,y, getHeatMap(g_map[x][y].countInfected));
        break;
        case 3:
          drawPixel(g_screen, x,y, getHeatMap(g_map[x][y].distToCave));
        break;
      }
    }
  }
  
  
  drawPixel(g_screen, g_bats[g_batSelection].x,g_bats[g_batSelection].y, 0x0000FF00);   
  
  //sprintf(g_buff,"R+ L-, Wheel: %s",ACTIONS[g_selection]);
  //sprintf(g_buff,"%d %d %d",r,g,b);
  snprintf(g_buff,BUFF_SIZE, "time: %d %d debug:%d", g_time, isWinter(), g_debug);
  g_message = TTF_RenderText_Solid( g_font, g_buff, textColor );
  apply_surface( MAP_SIZE_X, 0, g_message, g_screen);
  SDL_FreeSurface(g_message);
  
  sprintf(g_buff,"Live Bats: %d", g_totalBatCount);
  g_message = TTF_RenderText_Solid( g_font, g_buff, textColor );
  apply_surface( MAP_SIZE_X, 30, g_message, g_screen);
  SDL_FreeSurface(g_message);
  
  sprintf(g_buff,"#1: %d,%d f:%d i:%d", g_bats[g_batSelection].x, g_bats[g_batSelection].y, g_bats[g_batSelection].fat, g_bats[g_batSelection].infected);
  g_message = TTF_RenderText_Solid( g_font, g_buff, textColor );
  apply_surface( MAP_SIZE_X, 60, g_message, g_screen);
  SDL_FreeSurface(g_message);
  
  
  
  if( SDL_Flip( g_screen ) == -1 )
  {
    printf("UpdateFAIL\n");
  }

  return 0;
}

int nearestBat(int x, int y)
{
  int i;
  int target=0;
  int nearest = MAP_SIZE_X + MAP_SIZE_Y;
  int dist;
  
  for(i=0; i<MAX_NUM_BATS; i++)
  {
    if(g_bats[i].state == STATE_DEAD)
      continue;
  
    dist = abs(x - g_bats[i].x) + abs(y - g_bats[i].y);
    if( dist < nearest)
    {
      target = i;
      nearest = dist;
    }
  }
  return target;
}

//Called in Main
int handleKeyStroke()
{
  int sym = g_event.key.keysym.sym;
  //int mod = g_event.key.keysym.mod; //meta-keys, ctrl shift.  unused in this one

  //printf("testing keys: %d %c\n",sym,sym);

  switch(sym)
  {
  case SDLK_ESCAPE:
    exit(0);
  case ' ':
    g_paused = ~g_paused;
    break;
  case 'b':
    ;
    break;
  default:
    ;
  }
  return 0;
}

int handleEvent(int eventType)
{
  Uint32 color;
  switch(eventType)
  {
  case SDL_QUIT:
    printf("event: quit\n");
    return -1;
    break;

  case SDL_KEYDOWN:
    handleKeyStroke();
    break;

  case SDL_MOUSEMOTION:   

    if( g_event.button.button == 1) //Left mouse down
    {
      
      switch(g_selection)
      {
        case 0:
          color = 0x000000ff;
          break;
        case 1:
          color = 0x0000ff00;
          break;
        case 2:
          color = 0x00ff0000;
          break;
        default: ;
      }
      drawPixel(g_screen,g_event.motion.x,g_event.motion.y, color);
    }
    
    break;
    case SDL_MOUSEBUTTONDOWN:  
    if( g_event.button.button == 1) //Left mouse down
    {
      //Find nearest bat, change g_batSelection to that bat's index
      g_batSelection = nearestBat(g_event.button.x,g_event.button.y);
    }
    if( g_event.button.button == 3) //Right
    {
      
    }

    if( g_event.button.button == 4) //mousewheel down
    {
      g_selection = philMod(g_selection-1, SELECTIONMAX);
      g_debug++;
    }
    if( g_event.button.button == 5) //mousewheel up
    {
      g_selection = philMod(g_selection+1, SELECTIONMAX);
      g_debug--;
    }

  default:
    ;
  }
   return -1;
}



// MAIN!
int main( int argc, char* args[] )
{
  int quit = 0;

  if( init() != 0 )
  {
    return -1;
  }

  while( quit != 1 )
  {
    while( SDL_PollEvent( &g_event ) )
    {
      quit = handleEvent(g_event.type);
    }
    if(advanceTime() == -1)
     {
       printf("error advancing time, bailing\n");
       return -1;
     }
    animation();
  }
  clean_up();

  return 0;
}
